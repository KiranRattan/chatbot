var express = require('express');
var router = express.Router();

 var Messages = require("./message");
 var { History, Session } = require("./database");
  var SessionManagement = require("./session-management");
 var HistoryMangement = require("./history");
  var Common = require("./Common")

 router.post('/welcomescreen', function(req, res, next) {
    GetWelcomeScreen(req,res);
  });


function GetFullFillmentMessage(key) {
  var length = Messages.Welcome.FullFillment[key].length;
  if (length == 1)
      return Messages.Welcome.FullFillment[key][0];
  else {
      var index = Common.GenerateRandomNumber(length - 1);
      return Messages.Welcome.FullFillment[key][index];
  }
}

function GetWelcomeScreen(req, res) {
  var data = req.body.queryResult;
  var ouputContext = data.queryText;
  if(ouputContext && ouputContext!="Hi")
  {
      res.json({
          "fulfillmentText":Messages.Welcome.ValidationMessage.Error,
          "payload": {
              "google": {
                  "richResponse": {
                      "items": [
                          {
                              "simpleResponse": {
                                  "textToSpeech": Messages.Welcome.ValidationMessage.Error
                              }
                          }
                      ],
                      "suggestions": [
                          {
                              "title": Messages.Welcome.FullFillment.SuggestionChip.option1
                          },
                          {
                              "title": Messages.Welcome.FullFillment.SuggestionChip.option2
                          }
                      ],
                  }
              }
          }
      });
      return;
 }
  HistoryMangement.LogHistory("Welcome", req.body.session);
  var flag = true;
  if (req.body.originalDetectIntentRequest.source) {
      if (req.body.originalDetectIntentRequest.source == "google") {
          if (req.body.originalDetectIntentRequest.payload) {
              if (req.body.originalDetectIntentRequest.payload.user) {
                  if (req.body.originalDetectIntentRequest.payload.user.profile) {
                      SessionManagement.SetSession(req.body.session, req.body.originalDetectIntentRequest.payload.user.profile.displayName, null, null)
                      flag = false;
                  }
              }
          }
      }
  }
  else {
      flag = false;
  }
  if (flag) {
      SessionManagement.SetSession(req.body.session, null, null, null);
  }
  
  res.json({
      "fulfillmentText": GetFullFillmentMessage("SimpleResponse"),
      "payload": {
          "google": {
              "richResponse": {
                  "items": [
                      {
                          "simpleResponse": {
                              "textToSpeech": GetFullFillmentMessage("SimpleResponse")
                          }
                      }
                  ],
                  "suggestions": [
                      {
                          "title": Messages.Welcome.FullFillment.SuggestionChip.option1
                      },
                      {
                          "title": Messages.Welcome.FullFillment.SuggestionChip.option2
                      }
                  ],
              }
          }
      }
  });
}
module.exports = router;

