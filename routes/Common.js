function GenerateRandomNumber(max) {
    return Math.floor(Math.random() * (max - 0)) + 0;
}

function GenerateRandomNumberWithInRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function GetFullName(Name) {
    var nameArray= Name.split(" ");
    if (nameArray.length == 1) {
        Name = nameArray[0] + " " + nameArray[0];
    } else if (nameArray.length == 2) {
        Name = nameArray[0]+" " +nameArray[1];
    } else {
        Name = nameArray.shift(1)+" " +nameArray.join(" ");
    }
    return Name;
}

var phrase1=["red", "black","green", "pink"];
var phrase2=["table", "bag","pen"];

module.exports = {
    GenerateRandomNumber,
    GenerateRandomNumberWithInRange,
    GetFullName
}