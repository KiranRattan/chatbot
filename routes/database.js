const Sequelize = require('sequelize');

// Connecting to database
const sequelize = new Sequelize('mybank', 'mybank', 'BglghkgB', {
    host: '10.8.18.91',
    dialect: 'mysql',
    operatorsAliases: false
});

sequelize.authenticate().then(() => {
    console.log('Connection has been established successfully.');
}).catch(err => {
    console.error('Unable to connect to the database:', err);
});

// History Model
const History = sequelize.define("History", {
    Id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    IntentName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    SessionId: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    CreatedOn: {
        type: Sequelize.DATE,
        allowNull: false,
    },

    IsActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
    }
}, {
    timestamps: false
});

// Session Model
const Session = sequelize.define("Session", {
    Id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    SessionId: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    Name: {
        type: Sequelize.STRING,
    },
    DOB: {
        type: Sequelize.DATE,
    },
    IsActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
    }
}, {
    timestamps: false
});

sequelize.sync({
    logging: false
}).then(() => {
    console.log(`Database & tables created!`)
});

module.exports = {
    History,
    Session
}