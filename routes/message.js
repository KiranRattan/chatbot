module.exports = {
    Welcome: {
        FullFillment: {
            SimpleResponse: ["Hi! I'm Pennie, your Customer Relations Officer. Welcome to LASCO Microfinance Limited. I see you're interested in a loan. Let's get started, tell us the loan you are interested in.To check an existing application please click on Check Status."],
            SuggestionChip: {
                option1: "Apply for Personal Loan",
                option2: "Check Status"      
            }
        },
        ValidationMessage:{
            Error:"Sorry! I did not understand your response.I missed that, say that again?"
        }
    }
}