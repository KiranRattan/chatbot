var { History, Session } = require("./database");
var moment = require("moment");

//Set Session
function SetSession(sessionId, name, dob) {
    Session.findOne({ where: { SessionId: sessionId } }).then(result => {
        if (result) {
            if (name!=null || name!=undefined) {
                result.Name = name;
            }
            if (dob) {
                var date = moment(dob).format("YYYY-MM-DD");
                result.DOB = date;
            }
            result.save();
        }
        else {
            Session.create({ SessionId: sessionId, Name: name, DOB: dob, IsActive: true });
        }
    });
}

//Get Session
function GetSession(sessionId) {
    return Session.findOne({ where: { SessionId: sessionId } });
}
module.exports = {
    SetSession,
    GetSession
}